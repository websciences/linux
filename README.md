# TNSI Cours sur l'utilisation de Linux 

## Présentation

Ceci est un petit cours sur Linux pour vous initier aux bases de Linux.  
Je me suis plus que fortement inspiré du [cours de l'université de Lille 1](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/tree/master/bloc3?ref_type=heads)

## Contenu de ce cours :
- Une présentation succincte des instructions de base de Linux [Cours sur Linux](TNSI_Linux_TD01.ipynb)
- Un premier TD de mise en pratique de ces instrutions accompagné par l'enseignant [TD01](TNSI_Linux_TD01.ipynb)
- Un second TD de travail en autonomie sur ce sujet [TD02](TNSI_Linux_TD02.ipynb)


Sources : 
- [cours du DIU informatique de l'université de Lille 1](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/tree/master/bloc3?ref_type=heads)

<center><img src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"/></center>

Ce document est mis à disposition selon les termes de la [licence Creative commons :](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) <br/>
Attribution obligatoire - Pas d’utilisation commerciale - Partage dans les mêmes conditions.<br/>
email de votre enseignant : <websciences@free.fr><br/>
Przyszczypkowski Jean-Marc , 1ere & Terminale NSI Lycée Charlotte Perriand 59258 Genech <br/>